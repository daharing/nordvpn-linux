#!/usr/bin/env bash

# This scripts makes that assumption that it is run from the nordvpn-linux folder (the working directory )

# Install Dependencies
sudo apt-get update
sudo apt-get install iptables openvpn python2.7 unzip

# Download Latest Files from NordVPN
wget -N https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
unzip -o -d setup_files/Nord_openVPN_Servers/ ovpn.zip

# Pre-Install Cleanup
sudo rm ovpn.zip
sudo rm -Rf ~/.NordVPN/

# Setup Installation Directory
mkdir -p ~/.NordVPN/
mkdir -p ~/.NordVPN/Backups #Because git doesn't track empty folders and this folder will always be empty (ideally)

# Copy Files to Installation Directory
cp -rf setup_files/* ~/.NordVPN/
cp -rf README.md ~/.NordVPN/README.md

# Ensure scripts are executable
sudo chmod 750 ~/.NordVPN/*.sh
sudo chmod 750 ~/.NordVPN/Scripts/*.sh

# Cleanup
rm -f ~/.NordVPN/setup.sh

echo "Finished"
