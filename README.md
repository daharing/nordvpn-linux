# INSTALL
From the folder that contains the VPN files, type 
```sh
sudo ./install.sh
```
This will setup the necessary items for the VPN scripts.

## WARNING
If you are running this on a non-ubuntu machine, you must comment out the 'sudo apt update' and 'sudo apt install' lines in the install script. If you do this, ensure that the software being installed on that line is already installed on your system. See the Dependencies section 

## Dependencies
The list of packages that need to be installed are as follows:
 - python2.7/python2
 - openvpn
 - unzip
 - iptables

# UPDATE
To update to the latest Nord VPN server configurations (openvpn configuration files used by Nord VPN) available from Nord VPN, type
```sh
cd ~/.NordVPN/
./update.sh
```
This should be done periodically or when connecting to a server fails. If using portable mode, this must be done before the VPN can be used.

# START VPN

## Example Script 
A default/example script was created and can be used to make a quick connection. Simply type 
```sh
cd ~/.NordVPN/ && sudo ./vpnCA185_Example.sh
```
Otherwise, create a copy of this script and modify the VPN server IP address and the openvpn config file path parameters to tailor the script to your needs (see below).

## Server Picker Script Method (python)

1. Make sure python2.7 installed 
   ```sh
   sudo apt-get update
   sudo apt-get install python2.7
   ```

2. Type 
   ```sh
   cd ~/.NordVPN/ && python2.7 server_script_generator.py
   ```
3.  Follow the prompts and the script will pick the best server for your location and output the commands needed to start the VPN. Simply copy and paste the outputted commands into a terminal. Additionally, there are configuration variables in the script that may be modified to alter the script's behavior.

## Nord VPN Script Method (Manual Method)

1. Go to [https://nordvpn.com/servers/](https://nordvpn.com/servers/) and pick a server (click on the "Recommended Server" button). Pick the country to VPN to, the server type (default to 'Standard'), and the protocol (default to 'TCP').

2. Copy the resulting server name (ie "us2349").

3. Open the terminal and type 
   ```sh
   nano ~/.NordVPN/Nord_openVPN_Servers/ovpn_tcp/<copiedServerName>
   ``` 
   and press Tab to autocomplete the file name. It should look something like "us2349.nordvpn.com.tcp.ovpn" or "us2349.nordvpn.com.udp.ovpn" depending on the protocol chosen in step 1. **Note, replace "ovpn_tcp" with "ovpn_udp" if using the udp protocol.**

4. The nano text editor will open a file. The fourth line should look something like "remote 207.189.30.220 443". Copy ONLY the IP address, which in this example would be "207.189.30.220".

5. Go to the install directory (~/.NordVPN) and use the nordVPN script 
   ```sh
   cd ~/.NordVPN
   sudo ./nordVPN.sh <serverIP> ~/.NordVPN/Nord_openVPN_Servers/ovpn_tcp/<copiedServerName>
   ```
   and press Tab to autocomplete the file name.

# TO STOP VPN

Press Ctrl + C to stop the VPN.

If, after disconnecting the VPN by pressing Ctrl + C, the internet is inaccessible (or any network/IP address), then the iptables failed to restore the firewall setting to pre-VPN state. To reset the iptables firewall, use the command below. 
```sh
cd ~/.NordVPN/ && sudo ./resetIPTables.sh
```
This will reset iptables to its default state.

# NOTES
### Portable Mode
It is not necessary to install. To use the VPN without installing it, navigate into the setup_files directory and start/stop/use the VPN in the same way as normal (except of course ignore cd~/.NordVPN/ commands~).

Run the following command to ensure the scripts are executable
```sh
sudo chmod 750 *.sh
sudo chmod 750 Scripts/*.sh
```
### IPTABLES
The current configuration of iptables is exported and saved when using the VPN. Upon stopping the VPN, this configuration is restored. Note that due to an iptables bug, it will often fail to export settings if iptables is in its default configuration (accept all connections). In this case, see below...
### IPTABLES Failed Restore
When using the VPN, all network interfaces will be blocked except those operating over the VPN. For example, if you are connected wireless to the internet/VPN and there is also an ethernet connection to another network, all traffic on the ethernet network will be blocked for security. To whitelist local IP/connections, use iptables commands to whitelist it or modify the FirewallConfigs/ipv4Config_template.
### Dev Notes
It needs to be developed/tested to see what happens when running the VPN_Firewall scripts (that use iptables) when ufw is enabled.
