#!/usr/bin/env bash

#Restore the firewall and system settings back to their pre-VPN states


if [ -f /etc/sysctl.conf ]; then
        sudo sed -i -e '/net.ipv4.ip_forward=1/d' /etc/sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.all.disable_ipv6 = 1/d' /etc/sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.default.disable_ipv6 = 1/d' /etc/sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.lo.disable_ipv6 = 1/d' /etc/sysctl.conf
        #sudo sed -i -e 's/net.ipv6.conf.eth0.disable_ipv6 = 1/d' /etc/sysctl.conf
else
    if [ ! -f /etc/sysctl.d/99-sysctl.conf ]; then
        echo "File not found"
    else
        sudo sed -i -e '/net.ipv4.ip_forward=1/d' /etc/sysctl.d/99-sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.all.disable_ipv6 = 1/d' /etc/sysctl.d/99-sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.default.disable_ipv6 = 1/d' /etc/sysctl.d/99-sysctl.conf
        sudo sed -i -e '/net.ipv6.conf.lo.disable_ipv6 = 1/d' /etc/sysctl.d/99-sysctl.conf
        #sudo sed -i -e 's/net.ipv6.conf.eth0.disable_ipv6 = 1/d' /etc/sysctl.d/99-sysctl.conf
fi
fi

sudo sysctl -p

#Clean any rules from iptables
sudo iptables -F && sudo iptables -X

#Import original iptables configurations
sudo bash -c "iptables-restore < ~/.NordVPN/Backups/iptables.conf_original"
sudo bash -c "ip6tables-restore < ~/.NordVPN/Backups/ip6tables.conf_original"

rm ~/.NordVPN/Backups/iptables.conf_original
rm ~/.NordVPN/Backups/ip6tables.conf_original

echo "Firewall Restored!"
