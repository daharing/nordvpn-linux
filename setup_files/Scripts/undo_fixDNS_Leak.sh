#!/usr/bin/env bash

#Dustin Haring
#This file undoes the fix from the fix DNS fix

#If the copy of the original file does not exist, someone deleted it. Do not restore original
if [ ! -f ~/.NordVPN/Backups/resolv.conf ]; then

    echo "FAILED undo_fixDNS_Leak: Backups/resolv.conf does not exist. Unable to restore original file to pre-VPN state."
else

#Set the attributes allow modification of file
sudo chattr -i /etc/resolv.conf

sudo cp ~/.NordVPN/Backups/resolv.conf /etc/resolv.conf

#Set the attributes to disallow modification of file
#Don't need this because this will disallow the system to edit this file as needed during normal operation
#sudo chattr +i /etc/resolv.conf

#Delete our copy of the file
rm ~/.NordVPN/Backups/resolv.conf

echo Done!

fi
