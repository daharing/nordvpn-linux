#!/usr/bin/env bash
 
#Dustin Haring
#This file sets the DNS servers to NordVPN DNS servers. 

#if this file exists, then the undo script was never called and the copy of the
#original file already exists. DO NOT OVERWRITE
if [ ! -f ~/.NordVPN/Backups/resolv.conf ]; then

    sudo cp /etc/resolv.conf ~/.NordVPN/Backups/resolv.conf
fi

#Set the attributes allow modification of file
sudo chattr -i /etc/resolv.conf

#delete the file now that is it not locked
sudo rm -r /etc/resolv.conf

#Create new file with the valid DNS servers
sudo echo "nameserver 103.86.99.100" > /etc/resolv.conf
sudo echo "nameserver 103.86.96.100" >> /etc/resolv.conf

#Set the attributes to disallow modification of file
sudo chattr +i /etc/resolv.conf

#echo Please reboot your computer. 
#echo
